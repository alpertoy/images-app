import { ImageService } from './../../services/image.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-image',
  templateUrl: './search-image.component.html',
  styleUrls: ['./search-image.component.css']
})
export class SearchImageComponent implements OnInit {

  imageName: string;

  constructor(private imageService: ImageService) {
    this.imageName = '';
   }

  ngOnInit(): void {
  }

  searchImages() {

    if(this.imageName === '') {
      this.imageService.setError('Enter a text to search');
      return;
    }

    this.imageService.postSearchedTerm(this.imageName);
  }

}
