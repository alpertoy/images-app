import { Subscription } from 'rxjs';
import { ImageService } from './../../services/image.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-images',
  templateUrl: './list-images.component.html',
  styleUrls: ['./list-images.component.css']
})
export class ListImagesComponent implements OnInit {

  term = '';
  subscription: Subscription;
  imagesList: any[] = [];
  loading = false;
  imagesPerPage = 30;
  currentPage = 1;
  calculateTotalPages = 0;

  constructor(private imageService: ImageService) {
    this.subscription = this.imageService.getSearchedTerm().subscribe(data => {
      this.term = data;
      this.currentPage = 1;
      this.loading = true;
      this.getImages();
    });
   }

  ngOnInit(): void {
  }

  getImages() {
    this.imageService.getImages(this.term, this.imagesPerPage, this.currentPage).subscribe(data => {
      this.loading = false;
      if(data.hits.length === 0) {
        this.imageService.setError('Ops.. we did not find any results');
        return;
      }
      this.calculateTotalPages = Math.ceil(data.totalHits / this.imagesPerPage);
      this.imagesList = data.hits;
    }, error => {
      this.imageService.setError('Ops.. an error occured');
      this.loading = false;
    });
  }

  previousPage() {
    this.currentPage--;
    this.loading = true;
    this.imagesList = [];
    this.getImages();
  }

  nextPage() {
    this.currentPage++;
    this.loading = true;
    this.imagesList = [];
    this.getImages();
  }

  previousPageClass() {
   if (this.currentPage === 1) {
     return false;
   } else {
     return true;
   }
  }

  nextPageClass() {
    if (this.currentPage === this.calculateTotalPages) {
      return false;
    } else {
      return true;
    }
  }
}
