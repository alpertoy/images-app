import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private error = new Subject<string>();
  private searchedTerm = new Subject<string>();

  constructor(private http: HttpClient) { }

  setError(message: string) {
    this.error.next(message);
  }

  getError(): Observable<string> {
    return this.error.asObservable();
  }

  postSearchedTerm(term: string) {
    this.searchedTerm.next(term);
  }

  getSearchedTerm(): Observable<string> {
    return this.searchedTerm.asObservable();
  }

  getImages(term: string, imagesPerPage: number, currentPage: number): Observable<any> {
    const KEY = '20377360-eeb5551a0e8d536dbdf461537';
    const URL = 'https://pixabay.com/api/?key='+ KEY + '&q=' + term +
    '&per_page=' + imagesPerPage + '&page=' + currentPage;
    return this.http.get(URL);
  }
}
