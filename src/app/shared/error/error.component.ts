import { ImageService } from './../../services/image.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit, OnDestroy {

  text = '';
  show = false;
  subscription: Subscription;

  constructor(private imageService: ImageService) {
    this.subscription = this.imageService.getError().subscribe(data => {
      this.showMessage();
      this.text = data;
    })
   }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
  }

  showMessage() {
    this.show = true;
    setTimeout(() => {
      this.show = false;
    }, 3000);
  }

}
