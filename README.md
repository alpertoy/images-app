# Images App

Images App is built in Angular where you can search images.

## Pixabay API

Pixabay API is used to fetch images in this app.

`https://pixabay.com`

## Live Demo

[Check here](https://alpertoy.gitlab.io/images-app/)
